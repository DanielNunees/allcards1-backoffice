export class Address {
    constructor(
        public id: number,
        public address: string,
        public city: string,
        public state: string,
        public postcode: string,
    ) { }
}
