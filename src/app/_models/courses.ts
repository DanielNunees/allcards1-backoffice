import { School } from './schools';

export class Course {
    constructor(
        public id: number,
        public name: string,
        public school: School,
        public details: boolean
    ) { }
}
