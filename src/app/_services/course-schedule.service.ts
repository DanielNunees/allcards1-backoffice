import { Injectable } from '@angular/core';
import { CourseSchedule } from '../_models';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CourseScheduleService {

  constructor(private http: HttpClient) { }


  getAll() {
    return this.http.get<CourseSchedule[]>(environment.apiUrl + `/course/schedule`);
  }

  getById(id: number) {
    return this.http.get(environment.apiUrl + `/course/schedule` + id);
  }

  register(course: CourseSchedule) {
    return this.http.post(environment.apiUrl + `/course/schedule`, course);
  }

  update(course: CourseSchedule) {
    return this.http.put(environment.apiUrl + `/course/schedule` + course.id, course);
  }

  delete(id: number) {
    return this.http.delete(environment.apiUrl + `/course/schedule` + id);
  }
}
