import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from '../auth/auth.guard';
import { SchoolsComponent } from './schools/schools.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseScheduleComponent } from './course-schedule/course-schedule.component';
import { CustomerComponent } from './customer/customer.component';
import { BillingsComponent } from './billings/billings.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    children: [{
      canActivateChild: [AuthGuard],
      path: 'dashboard', component: DashboardComponent },
      { path: 'schools', component: SchoolsComponent },
      { path: 'courses', component: CoursesComponent },
      { path: 'course-schedule', component: CourseScheduleComponent },
      { path: 'customers', component: CustomerComponent },
      { path: 'billing', component: BillingsComponent },
      { path: '', component: DashboardComponent }
    

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
