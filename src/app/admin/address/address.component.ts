import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'address-form',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  @Input() parentForm: FormGroup;
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    
  }

  get address(){return this.parentForm.get("address")};
  get city(){return this.parentForm.get("city")};
  get state(){return this.parentForm.get("state")};
  get postcode(){return this.parentForm.get("postcode")};



}
