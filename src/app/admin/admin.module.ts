import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SchoolsComponent } from './schools/schools.component';
import { NavBarComponent } from '../_components/nav-bar/nav-bar.component';
import { SideMenuComponent } from '../_components/side-menu/side-menu.component';
import { CoursesComponent } from './courses/courses.component';
import { AddressComponent } from './address/address.component';
import { NgbCollapseModule, NgbAccordionModule, NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { CourseScheduleComponent } from './course-schedule/course-schedule.component';
import { CustomerComponent } from './customer/customer.component';
import { BillingsComponent } from './billings/billings.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

@NgModule({
  declarations: [
    DashboardComponent,
    AdminComponent,
    SchoolsComponent,
    NavBarComponent,
    SideMenuComponent,
    CoursesComponent,
    AddressComponent,
    CourseScheduleComponent,
    CustomerComponent,
    BillingsComponent,
    ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbCollapseModule,
    NgbAccordionModule,
    AngularDateTimePickerModule,
    NgbPaginationModule
  ]
})
export class AdminModule { }
