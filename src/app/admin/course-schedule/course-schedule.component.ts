import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CoursesService } from 'src/app/_services/courses.service';
import { CourseScheduleService } from 'src/app/_services/course-schedule.service';
import { CourseSchedule, School, Course } from 'src/app/_models';

@Component({
  selector: 'course-schedule',
  templateUrl: './course-schedule.component.html',
  styleUrls: ['./course-schedule.component.scss']
})
export class CourseScheduleComponent implements OnInit {
  courseScheduleForm: FormGroup;
  schedules: CourseSchedule[] = [];
  schools: School[] = [];
  courses: Course[] = [];
  public collapse: boolean[] = [];
  public page = 1;
  public pageSize = 5;

  date: Date = new Date();
  date1: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd/MM/yyyy h:mm a',
    defaultOpen: false
  }
  constructor(private fb: FormBuilder,
    private courseScheduleService: CourseScheduleService,
    private courseService: CoursesService) { }

  ngOnInit() {
    this.courseScheduleForm = this.fb.group({
      course_id: ['', Validators.required],
      start_time: ['', Validators.required],
      end_time: ['', Validators.required],
    });
    this.getAvailableCourses();
    this.getAvailableSchedules();
  }

  getAvailableCourses() {
    this.courseService.getAll().subscribe(
      data => {
        this.courses = data;
        this.details(data.length);
      }
    )
  }

  GetFormattedDate(date: Date) {
    return new Date(date).toLocaleString().slice(0, 19).replace('T', ' ')

  }

  getAvailableSchedules() {
    this.courseScheduleService.getAll().subscribe(
      data => {
        this.schedules = data;
        console.log(this.schedules);
      })
  }

  details(i) {
    this.collapse[i] = this.collapse[i] ? false : true;
  }

  get course_id() { return this.courseScheduleForm.get("course_id"); }
  get start_time() { return this.courseScheduleForm.get("start_time"); }
  get end_time() { return this.courseScheduleForm.get("end_time"); }

  onSubmit() {
    console.log(this.courseScheduleForm);
    console.log(this.GetFormattedDate(this.start_time.value));
    var mydate = new Date(this.start_time.value);
    var str = mydate.toString();


    if (!this.courseScheduleForm.valid)
      return;
    this.courseScheduleService.register(this.courseScheduleForm.value).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      })
  }

}
