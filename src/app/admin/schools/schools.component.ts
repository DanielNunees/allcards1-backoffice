import { Component, OnInit } from '@angular/core';
import { School } from 'src/app/_models';
import { SchoolsService } from 'src/app/_services/schools.service';
import { first } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.scss']
})
export class SchoolsComponent implements OnInit {

  schools: School[] = [];
  schoolForm: FormGroup;
  public collapse: boolean[] = [];
  constructor(private schoolService: SchoolsService, private fb: FormBuilder) { }


  ngOnInit() {

    this.getAllSchools();

    this.schoolForm = this.fb.group({
      name: ["", Validators.required],
      phone: ["", Validators.required],
      email: [""],
      description: [""],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      postcode: ['', Validators.required],
    })
  }

  getAllSchools() {
    this.schoolService.getAll().subscribe(
      data => {
        this.details(data.length);
        this.schools = data;
      }
    );
  }
  details(i) {
    this.collapse[i] = this.collapse[i] ? false : true;
  }

  onSubmit() {
    this.schoolService.register(this.schoolForm.value).subscribe(
      data => {
        this.getAllSchools();
      },
      error => {
        console.log(error);
      }
    );
  }

  get name() { return this.schoolForm.get("name") };
  get phone() { return this.schoolForm.get("phone") };
  get email() { return this.schoolForm.get("email") };

}
