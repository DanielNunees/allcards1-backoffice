import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoursesService } from 'src/app/_services/courses.service';
import { SchoolsService } from 'src/app/_services/schools.service';
import { School, Course } from 'src/app/_models';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit {
  courseForm: FormGroup;
  schools: School[] = [];
  courses: Course[] = [];
  public collapse: boolean[] = [];
  //isCollapsed: boolean= false;

  constructor(private fb: FormBuilder,
    private coursesService: CoursesService,
    private schoolService: SchoolsService) { }

  ngOnInit() {
    this.courseForm = this.fb.group({
      course_name: ["", Validators.required],
      price: ["", Validators.required],
      school_id: ["", Validators.required],
      description: [""],
    });
    this.getAvailableSchools();
    this.getAvailableCourses();
    
  }

  details(i) {
    this.collapse[i] = this.collapse[i]?false:true;
  }

  getAvailableSchools() {
    this.schoolService.getAll().subscribe(
      data => {
        this.schools = data;
      })
  }

  getAvailableCourses() {
    this.coursesService.getAll().subscribe(
      data => {
        this.courses = data;
        this.details(data.length);
      }
    )
  }

  get course_name() { return this.courseForm.get("course_name") };
  get school_id() { return this.courseForm.get("school_id") };
  get description() { return this.courseForm.get("description") };
  get price() { return this.courseForm.get("price") };

  onSubmit() {
    this.coursesService.register(this.courseForm.value).subscribe(
      data => {
        console.log(data);
        this.getAvailableCourses();
      })
  }

}
